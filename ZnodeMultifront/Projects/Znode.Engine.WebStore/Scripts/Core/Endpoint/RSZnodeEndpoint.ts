﻿class RSZnodeEndpoint extends ZnodeBase {
    
    constructor() {
        super();
    }

    /*Nivi code Start*/
    GetMapURLSuccess(state, sku, callbackMethod) {
        super.ajaxRequest($("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    }
    GetStoresForStoreLocatorSuccess(state, sku, _userCord, callbackMethod) {
        super.ajaxRequest($("#apiURL").val() + "/DSPublishProduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    }
      
    GetStoresForCartSuccess(state, sku, _userCord, callbackMethod) {
        super.ajaxRequest($("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    }
    GetStockForSelectedStoreSuccess(state, sku, selectedstore, callbackMethod) {
        super.ajaxRequest($("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    }
    GetStoresSuccess(state, sku, _userCord, callbackMethod) {
        //   alert("endpoint");
        super.ajaxRequest($("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    }    
    GetCurrentSelectedStoreDetailsSuccess(state, sku, callbackMethod) {
        super.ajaxRequest($("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    }
    FillLocationDropdownsuccess(sku, callbackMethod) {
        super.ajaxRequest($("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/ALL/" + sku, Constant.GET, { "sku": sku }, callbackMethod, "json");
    }
    /*Nivi code End*/


}