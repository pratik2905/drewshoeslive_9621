﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Sample.Api.Model
{
    public class DSCustomListModel : BaseListModel
    {
        public List<DSCustomModel> InventoryList { get; set; }
    }

}
