﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Znode.Api.Custom.Helper;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Observer;

namespace Znode.Api.Custom.Service.Service
{
    public class DSSearchService : SearchService
    {
        #region private variable
        //private readonly IMongoRepository<ProductEntity> _ProductMongoRepository;
        #endregion
        string PortalIdDrew = ConfigurationManager.AppSettings["PortalIDDrew"];
        string PortalIdRos = ConfigurationManager.AppSettings["PortalIDRos"];
        string PortalIdBellini = ConfigurationManager.AppSettings["PortalIDBellini"];

        public override KeywordSearchModel FullTextSearch(SearchRequestModel model, NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            KeywordSearchModel searchResult = base.FullTextSearch(model, expands, filters, sorts, page);
            try
            {
                if (searchResult.Products != null)
                {
                    DSAttributeSwatchHelper attributeSwatchHelper = new DSAttributeSwatchHelper();
                    if (model.PortalId == Convert.ToInt32(PortalIdDrew))
                    {
                        attributeSwatchHelper.GetAssociatedConfigurableProducts(searchResult, model, "drewcolor");
                    }
                    else if (model.PortalId == Convert.ToInt32(PortalIdRos))
                    {
                        attributeSwatchHelper.GetAssociatedConfigurableProducts(searchResult, model, "roscolor");
                    }
                    else if (model.PortalId == Convert.ToInt32(PortalIdBellini))
                    {
                        attributeSwatchHelper.GetAssociatedConfigurableProducts(searchResult, model, "roscolor");
                    }
                    else
                    {
                        attributeSwatchHelper.GetAssociatedConfigurableProducts(searchResult, model, "drewcolor");
                    }
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Unable to FullTextSearch ErrorDetails: " + ex.Message + " Inner Exception: " + ex.InnerException + " StackTrace: " + ex.StackTrace, ZnodeLogging.Components.Search.ToString(), TraceLevel.Error);
            }                    
            return searchResult;
        }
    }
}
